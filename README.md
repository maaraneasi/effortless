# README #


### What is this repository for? ###

* This is a simple (cowboy coded) Greasemonkey extension for Efforts portal automation
* Version - Somewhere around 1, eternal beta...


### How do I get set up? ###

* Make sure the Greasemonkey extansion for Firefox is installed (https://addons.mozilla.org/cs/firefox/addon/greasemonkey/)
* Download Efforts.user.js
* Drag and drop the js file to the Firefox window.
* On the prompt, respond "Install user script"
The script is now installed.

**Verify the script is ready to go:**

* There is now a little monkey face on the FF bar, indicating the extension is installed - click the arrow next to it and select "Manage User Scripts..."
* You will see script "Efforts 1" listed
* Click the "Options button" 
* Make sure the "Included pages" on the "User settings" tab contains page 
"https://odapps.oracleoutsourcing.com/pls/htmldb/*"
!!! Please note the asterix at the end of the url!!!
* If not, add the url as above
* You are good to go - save the settings and reload the Efforts portal - if everything is ok, an input prompt will pop up....

### How do I update the script? ###

To be hones, I don't know...
Once you have downloaded the raw script from the Bitucket, there are few options:

One option is to go to C:\Users\<username>\AppData\Roaming\Mozilla\Firefox\Profiles\<profile name>\gm_scripts\Efforts
and replace the script with the new copy

Second option is to:
* Click the arrow next to the little monkey icon on the Firefox toolbar and select "Manage User Scripts..."
* You will see script "Efforts 1" listed
* Click the "Options button" 
* Click "Edit this user script" on the bottom of the window
* Replace the whole source code

Third option might be draggig and dropping the script file to the FF window, like when installing for the first time. (I haven't tested it!!)

### How to use it ###

Script expects comma ("**,**") separated list of values in format:

| 0  |  1 |    2    |  3   |     4    |   5  |   6  |
|----|----|---------|------|----------|------|------|
|RFC | SR | PRODUCT | TYPE | DURATION | DATE | NOTE |

==> if you are filling in your time tracking timesheet (xls), export it as comma separated csv, so you can take lines one by one 

If RFC is empty but SR entered, the input is considered to be SR
If RFC is entered but SR empty, the input is considered to be RFC
If RFC is empty and SR empty, the input is considered to be PROJECT or SL

~~The duration is expected to be in minutes and rounded to nearest number of hours (ex. 45 minutes is taken as 1 hour of work)~~
Date is filled dynamically (**input expected to be dd.mm.yyyy**), converted to dd-MON-yyyy.
Start and end time is calculated based on duration in following logic:
>* if the number of worked hours is 1, the start time will be placed at random between 09am - 17pm 
>* if number of worked hours is for example 6, the start time will be 09am - 11am  
>* etc...

All mandatory fields are filled in to ensure the form can be submitted.
If there is a problem with the script or the values, nothing is filled in (the script is not invasive)

The script doesn't submit any changes - it only fills in the values.

### Known bugs ###
* After saving the filled form, the next screen with confirmation "Action processed" comes out. The Efforts portal expects that you press the "Enter new task" othervise the entry is not saved, but will be overwritten. To avoid this issue, the prompt for inserting the CSV line will be not displayed if the text "Action processed" will be found on the page.
* There is nearly no exception handling
* Code is dirty
* Only basic checks of the entered values
* The pop-up prompt is the only way how to get the input - I have been experimenting with adding additional form to the web page to make it more fancy, but there were issues with submitting the form to fill in the values ---> there is no way how to redraw & fill the form without refreshing the page (maybe ajax or jquery in future)... 

### Contribution guidelines ###

* Report any bugs & enhancement request
* Or get GIT on Bitbucket access to update and push requests

### Who do I talk to? ###

* Marek