// ==UserScript==
// @name        Efforts
// @namespace   cz.maara
// @include     https://odapps.oracleoutsourcing.com/pls/htmldb/*
// @version     1.10
// @grant    GM_getValue
// @grant    GM_setValue
// @grant    GM_listValues
// ==/UserScript==

//CSV line 7 tokens:
//,,N/A,Project Work,45,10.11.2015,Technip Storage,,,
//RFC SR PRODUCT TYPE DURATION DATE NOTE
// 1   2    3      4     5       6    7
//  ,3-5D58127 ,N/A,Incident Management,45,13.11.2015,,,,

console.log("Loading..."); //Log the script entry point 
var queue_name = 'TECH'; // Queue name as in the dropdown (can be cheanged if needed)


document.addEventListener('load', main()); // Page loaded, engage the main function

function main() {
  console.log("Starting");
  console.log(get_reload());

  if (!check_stage()) { //No "Action processed" banner found" && "Enter the Task Execution Details" found"
  	if (get_reload() != 1) { // IF GM_getValue is not 1, we can set the queue, reload the list and set GM_getValue to 1
  	set_queue(queue_name);	
  	}
  else if (get_reload() == 1) { // IF GM_getValue is exactly 1, process the page normally without any reloads
  		init_elements();
  		input_handler("1");
  		//create_form();
  		rnd_complexity();
  		hide_elements();
  	}
  else { //Expect the unexpected....and return....
  	return;
  }
 }
  return true;
}

//Set the "reloaded" GM variable to passed value (should be set to 1)
function set_reload(reload) {
	GM_setValue("reloaded", reload);
	console.log("GM_RELOADED_WRITE: " +reload); 
	return true;
}

//Get the value of "reloaded" variable
function get_reload() {
	reloaded = GM_getValue("reloaded");
	console.log("GM_RELOADED_READ: " +reloaded); 
	return reloaded;
}

// Now this is nterresting.... The Worktype dropdown menu (P4_QUE_WT_MAP_ID), lists the values only after Queue name (P4_QUEUE_CODE) manual change.
// To get the values, we set the P4_QUEUE_CODE dropdown to queue_name variable and invoke its "onchange()" event so the Worktype values are populated.
//THIS CAUSES reload of the page hence the set_reload sets the GM Variable "reloaded" to 1
function set_queue(queue_name) {
	document.getElementById("P4_QUEUE_CODE").value = queue_name;
	document.getElementById("P4_QUEUE_CODE").onchange();
	set_reload(1);
	return true;
}

//Function checks the content of the page.
//It looks for "Enter the Task Execution Details" and "Action Processed." strings.
// If "Action Processed." was not found but "Enter the Task Execution Details" WAS FOUND. The main prompt can be displayed.
// If "Action Processed." was not found but "Enter the Task Execution Details" WAS NOT FOUND. It means the page is not ready
// IF "Action Processed." is found, the reload GM Variable is set to 0 (as we are starting over)
function check_stage() {
var xpathResult = document.evaluate("(//text()[contains(., 'Action Processed.')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
var xpathResult2 = document.evaluate("(//text()[contains(., 'Enter the Task Execution Details')])[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
var node=xpathResult.singleNodeValue;
var node2=xpathResult2.singleNodeValue;
if (node==null) { //No "Action processed found"
	console.log("Action Processed found: " +node);
	if (node2!==null) { //"Enter the Task Execution Details found"
		console.log("Enter the Task Execution Details found: " +node2);
		console.log("Stage false");
    	return false; //OK - ready to display the prompt
	}
	else {
		console.log("Stage true");
		return true;	
	}
}
else
	console.log("Stage true");
	set_reload(0);
    return true;
}

// This displays the input prompt expecting the CSV line.
// Once confirmed, the line is splitted using "," and based on the tokens the type of the line is
function input_handler(in_data) {

  var user_input = prompt ("Please enter CSV line:",""); //show prompt for number of hours
  //var user_input = in_data;
  console.log(user_input);
  var arguments = user_input.split(","); //split!
  var i_sr = arguments[0];
  var i_rfc = arguments[1];
  var i_product = arguments[2];
  var i_type = arguments[3];
  var i_duration = arguments[4];
  var i_date = arguments[5];
  var i_note = arguments[6];

//DEBUG LOOP for listing variables
  console.log("Input arguments:"); 
  for (var i=0; i < arguments.length; i++) {
   console.log("Argument " +i+" : " +typeof(arguments[i]) +" : " +arguments[i]);    
  }
//END DEBUG LOOP for listing variables

//RFC or SR or PROJECT?? Lets check which tokens are empty, and which are not and call the corresponding function for filling in the form
  if ((arguments[0].length == 0) && (arguments[1].length !== 0 )) { //RFC not null
    console.log("Considered RFC"); 
    set_rfc(i_rfc, i_date, i_note, i_duration, i_type); 
  }
  else if ((arguments[0].length !== 0 ) && (arguments[1].length == 0 )) { //RFC null, SR not null
    console.log("Considered SR"); 
     set_SR(i_sr, i_date, i_note, i_duration, i_type); 
  }
  else if ((arguments[0].length == 0) && (arguments[1].length == 0 )) { //BOTH null, lets check further
    console.log("Considered Project/Holiday");
    if (arguments[3] == 'Holiday') {
    	console.log('Its holiday');
    	set_holiday(i_date, i_note, i_duration, i_type);
    }
    else { // We are not sure, lets call it project...
    	console.log('Its project');
  		set_project(i_date, i_note, i_duration, i_type);  	
    }
  }
  else { // Something was wrong
    console.log("Considered UNKNOWN");
    alert("Could not recognize work type....")
  }
  return;
}

function set_rfc(rfc_no, date, note, duration, worktype) { // Processes information for filling in RFC's
  console.log("Set_Rfc arguments:"); 
  console.log("Set_RFC RFC: " +rfc_no); 
  console.log("Set_RFC DATE: " +date); 
  console.log("Set_RFC NOTE: " +note); 
  console.log("Set_RFC DURATION: " +duration); 
  rfc_no = rfc_no.replace(/\s/g, ''); // Remove blanks and weird characters from the RFC number
  var roundHours = round_hours(duration);
  var numhours = roundHours[0];
  console.log("Set_RFC HOURS: " +numhours);
  var numminutes = roundHours[1];
  console.log("Set_RFC Minutes: " +numminutes);
  document.getElementById("P4_TAR_NUMBER").value = rfc_no;
  document.getElementById("P4_EXECUTION_TYPE_2").click();
  document.getElementById("P4_QUE_WT_MAP_ID").value = "1041";
  document.getElementById("P4_COMMENTS").value = note;
  var date_processed = date_convert(date);
  console.log("Set_RFC FANCY DATE: " +date_processed);
  date_string_builder(date_processed,numhours,numminutes);

return;
}

function set_project(date, note, duration, worktype) { // Processes information for filling in RFC's
  console.log("Set_Project arguments:"); 
  console.log("Set_Project DATE: " +date); 
  console.log("Set_Project NOTE: " +note);
  console.log("Set_Project WORKTYPE: " +worktype); 
  console.log("Set_Project DURATION: " +duration); 
  var roundHours = round_hours(duration); //Convert minutes to hours and minutes
  var numhours = roundHours[0];
  console.log("Set_Project HOURS: " +numhours);
  var numminutes = roundHours[1];
  console.log("Set_Project Minutes: " +numminutes);
  document.getElementById("P4_TAR_NUMBER").value = "";
  document.getElementById("P4_EXECUTION_TYPE_5").click();
  document.getElementById("P4_COMMENTS").value = worktype +" | " +note;
  //Now lets have a look at the worktypes. We care about "Shft lead" and "Project work". The rest is an useless bullshit
  console.log("Set_Project worktype: " +worktype);
  if (worktype == "Shift Lead") {
  	console.log("Set_Project worktype evaluate: " +worktype);
  	document.getElementById("P4_QUE_WT_MAP_ID").value = "1056";
  	document.getElementById("P4_QUE_WT_MAP_ID").click();
  }
  else if (worktype == "Project Work") {
  	console.log("Set_Project worktype: " +worktype);
  	document.getElementById("P4_QUE_WT_MAP_ID").value = "1091";
  }
  else {
  	document.getElementById("P4_QUE_WT_MAP_ID").value = "1092"; //If we don't know, let's put there "Others:"
  }
  var date_processed = date_convert(date);
  console.log("Set_Project FANCY DATE: " +date_processed);
  date_string_builder(date_processed,numhours,numminutes);

return;
}

//Holidays are a special case...
function set_holiday(date, note, duration, worktype) { // Processes information for filling in RFC's
  console.log("Set_Holiday arguments:"); 
  console.log("Set_Holiday DATE: " +date); 
  console.log("Set_Holiday NOTE: " +note);
  console.log("Set_Holiday WORKTYPE: " +worktype); 
  console.log("Set_Holiday DURATION: " +duration); 
  var roundHours = round_hours(duration); //Convert minutes to hours and minutes
  var numhours = roundHours[0];
  console.log("Set_Holiday HOURS: " +numhours);
  var numminutes = roundHours[1];
  console.log("Set_Holiday Minutes: " +numminutes);
  document.getElementById("P4_TAR_NUMBER").value = "";
  document.getElementById("P4_EXECUTION_TYPE_5").click();
  document.getElementById("P4_COMMENTS").value = worktype +" | " +note;
  document.getElementById("P4_QUE_WT_MAP_ID").value = "1699";
  var date_processed = date_convert(date);
  console.log("Set_Holiday FANCY DATE: " +date_processed);
  date_string_builder(date_processed,numhours,numminutes);

return;
}


function comment_filler(work_type, comments) {
  //We care for:
  // - Incident Management
  // - Project Work
  // - Shift Lead
  // Anything other is bypassed


  return true;
}

function set_SR(sr_no, date, note, duration, worktype) { // Processes information for filling in RFC's
  console.log("Set_SR arguments:"); 
  console.log("Set_SR SR: " +sr_no); 
  console.log("Set_SR DATE: " +date); 
  console.log("Set_SR NOTE: " +note); 
  console.log("Set_SR DURATION: " +duration); 
  sr_no = sr_no.replace(/\s/g, '');  // Remove blanks and weird characters from the SR number
  var roundHours = round_hours(duration); //Convert minutes to hours and minutes
  var numhours = roundHours[0];
  console.log("Set_SR HOURS: " +numhours);
  var numminutes = roundHours[1];
  console.log("Set_SR Minutes: " +numminutes);
  document.getElementById("P4_TAR_NUMBER").value = sr_no;
  document.getElementById("P4_EXECUTION_TYPE_0").click();
  document.getElementById("P4_COMMENTS").value = note;
  document.getElementById("P4_QUE_WT_MAP_ID").value = "1041";
  var date_processed = date_convert(date);
  console.log("Set_SR FANCY DATE: " +date_processed);
  date_string_builder(date_processed,numhours,numminutes);

return;
}

//RFC, SR and Project functions call this function which generates timestamps which are then filled into "Actual Requested Date", "Actual Start Date", "Actual End Date"
function date_string_builder(date,numhours,numminutes) {
  var start_hour = time_builder(numhours); // We know only duration of the event. The time_builder takes care of the rest
  var end_hour = parseInt(start_hour) + parseInt(numhours); //Start hour is returned by the time_builder. The end hour is calculated based on duration
  console.log("Time builder start hour:" +start_hour);
  console.log("Time builder end hour:" +end_hour);
 // var timestamp_start = date +" " +start_hour +":" +numminutes;
  var timestamp_start = date +" " +start_hour +":00"; //Now we have string in format "01-JAN-2016 10:00"
  var timestamp_end = date +" " +end_hour +":" +numminutes; //...and end date in the same format.
  console.log("Time builder start timestamp:" +timestamp_start);
  start_date_filler(timestamp_start); //Insert data to the web form
  console.log("Time builder end timestamp:" +timestamp_end); 
  end_date_filler(timestamp_end); //Insert data to the web form
  return true;
}

// Converts minutes to hours & minutes and returns array
function round_hours(in_minutes) { 
console.log("Executing minutes to hours conversion: " +in_minutes); 
var minutes = in_minutes % 60; // remaining mnutes (62 minutes results in 2 minutes)
var hours = (in_minutes-minutes)/60;  //substract remaining minutes from the in_minutes and get hours
if (minutes < 10) { // Add zero to minutes (2 minutes --> 02 minutes). No need to care about hours as hours are processed by time_builder()
        console.log("Zero needed for " +minutes);
        minutes = "0" +minutes;
    }
 console.log("Minutes entry: " +minutes);   
 console.log("Minutes to hours: " +hours);  
 return [hours, minutes];  
}  

// Converts date from dd.mm.yyyy to dd-MON-YYYY
function date_convert(date) { 
   var monthNames = [
  "JAN", "FEB", "MAR",
  "APR", "MAY", "JUN", "JUL",
  "AUG", "SEP", "OCT",
  "NOV", "DEC"
  ];
  var date_elements = date.split("."); 
  var datestring = date_elements[0]+'-'+monthNames[date_elements[1]-1]+'-'+date_elements[2];

  return datestring;
}

// We know how many hours the event took. This function picks a random start hour within the business hours (starting at 9), based on how the even took long.
//EX. an event which took 1hr, can start anytime between 09-18
//but and event which took 8 hours (like Shift lead), can start only at 9 or 10 (1 hour for lunch).
function time_builder(numhours) { 
  // Math.floor(Math.random() * (max - min + 1)) + min
  console.log("Choosing best start time " +numhours +" hours");
  var hh = 0;
  var start_hour = parseInt(9);
switch (numhours) {
	case 0: hh = parseInt(Math.floor(Math.random() * (17 - 9 + 1)) + 9) ;
          console.log("Option 0"); 
          console.log("Choosed at random: " +hh); 
    break;
  case 1: hh = parseInt(Math.floor(Math.random() * (17 - 9 + 1)) + 9) ;
          console.log("Option 1"); 
          console.log("Choosed at random: " +hh); 
    break;
  case 2: hh = parseInt(Math.floor(Math.random() * (16 - 9 + 1)) + 9) ;
            console.log("Option 2"); 
    break;
  case 3: hh = parseInt(Math.floor(Math.random() * (15 - 9 + 1)) + 9)  ;
            console.log("Option 3"); 
    break;
  case 4: hh = parseInt(Math.floor(Math.random() * (14 - 9 + 1)) + 9)  ;
            console.log("Option 4"); 
    break;
  case 5: hh = parseInt(Math.floor(Math.random() * (13 - 9 + 1)) + 9)  ;
            console.log("Option 5"); 
    break;
  case 6: hh = parseInt(Math.floor(Math.random() * (12 - 9 + 1)) + 9)  ;
            console.log("Option 6"); 
    break;
  case 7: hh = parseInt(Math.floor(Math.random() * (11 - 9 + 1)) + 9) ;
            console.log("Option 7"); 
    break;
  case 8: hh = parseInt(Math.floor(Math.random() * (10 - 9 + 1)) + 9) ;
    break;
  default: hh = "0";
}
// Make fancy hours - we don't want 9 --> 09 is what we want!!
console.log("Need zero for " +hh +"?");
if (hh < 10) {
        console.log("Zero needed for " +hh);
        hh = "0" +hh;
    }
console.log("Start hour chosen + " +hh);

  return hh;
}

//Fills in start date & time to the form (these two dates are the same)
function start_date_filler(timestamp) {
  document.getElementById("P4_ACTUAL_REQUESTED_DATE").value = timestamp;
  document.getElementById("P4_ACTUAL_START_DATE").value = timestamp;
  return;
}

//Fills in the end date to the form
function end_date_filler(timestamp) {
  document.getElementById("P4_ACTUAL_END_DATE").value = timestamp;
  return;
}

//Fill random complexity as it a mandatory field
function rnd_complexity() {
 console.log("Generating Complexity")
 var num_choices = 3;
    var rand_no = Math.ceil(num_choices * Math.random());
    switch(rand_no) {
        case 1: output = "High";
                break;
        case 2: output = "Medium";
                break;
        case 3: output = "Low";
                break;  
        default: output = "%";
    }
  document.getElementById("P4_COMPLEXITY_LEVEL").value = output;
  console.log("Complexity level " +output);
  return
}

//Fill in mandatory fields
function hide_elements() {
    console.log("Hiding crap");
document.getElementById("P4_TYPE").value = "PRODUCTION";
document.getElementById("P4_ACTION_COMPLETED").value = "Yes";
document.getElementById("P4_SEVERITY").value = "2";
document.getElementById("P4_INSTANCE_NAME").remove();
document.getElementById("P4_EMERGENCY_FLAG").value = "No";
document.getElementById("P4_CUSTOMER_TYPE").value = "Standard";
document.getElementById("P4_NEW_TIME_ZONE").value = "EMEA";
document.getElementById("P4_COMMS_ID").remove();
document.getElementById("P4_INSTANCE_NAME").remove();
return true;
}


//Dummy placeholder TO DELETE
function init_elements() {
	console.log("Init");
	//document.getElementById("P4_QUEUE_CODE").value = "TECH";
	//  document.getElementById("P4_QUEUE_CODE").onchange();
	//var element = document.getElementById("P4_QUEUE_CODE").value = "TECH";
	//fireChangeEvent("P4_QUEUE_CODE");
	//element.fireEvent("onchange");
	//document.getElementById("P4_QUEUE_CODE").value = "OA";
	//document.getElementById("P4_QUEUE_CODE").click();
	//
	//document.getElementById("P4_QUEUE_CODE").click();
	//document.getElementById("P4_QUE_WT_MAP_ID").click();
	//document.getElementById("P4_QUE_WT_MAP_ID").value = "1041";
	//document.getElementById("P4_QUE_WT_MAP_ID").submit();
	console.log("Done init");
	return true;
}

//Experimental TO DELETE
function fireChangeEvent(elmId)
{
    var element = document.getElementById(elmId);
    if ('fireEvent' in element)
        element.fireEvent("onchange");
    else {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        element.dispatchEvent(evt);
    }
}   